package ru.tsc.apozdnov.tm.exception.user;

public class LoginExistException extends AbstractUserException {

    public LoginExistException() {
        super("Error!!! Login already exist!");
    }

}

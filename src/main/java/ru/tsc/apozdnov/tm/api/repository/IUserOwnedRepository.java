package ru.tsc.apozdnov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    M removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    M remove(@NotNull String userId, @NotNull M model);

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator comparator);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Sort sort);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

    void clear(@NotNull String userId);

}

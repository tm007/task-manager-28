package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.apozdnov.tm.api.service.IUserOwnedService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public @NotNull List<M> findAll(final @NotNull String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public boolean existsById(final @NotNull String userId, final @NotNull String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || userId.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public @NotNull M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || userId.isEmpty()) throw new IdEmptyException();
        Optional<M> model = Optional.ofNullable(repository.findOneById(userId, id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || userId.isEmpty()) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(userId, index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public int getSize(final @NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public @NotNull M removeById(final @NotNull String userId, final @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public M removeByIndex(final @NotNull String userId, final @NotNull Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || userId.isEmpty()) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public @NotNull M remove(final @NotNull String userId, final @NotNull M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.remove(userId, model);
    }

    @Override
    public M add(final @NotNull String userId, final @NotNull M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Override
    public @NotNull List<M> findAll(final String userId, final @NotNull Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(final @NotNull String userId, final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

}

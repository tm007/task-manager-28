# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Aleksandr Pozdnov

* **E-MAIL**: apozdnov@t1-consulting.com

* **E-MAIL**: apozdnov@tsconsulting.com

## SOFTWARE

* OpenJDK 8

* Intellij Idea Ultimate

* Windows 10 Pro 20H2

## HARDWARE

* **RAM**: 8Gb

* **CPU**: i5

* **HDD**: 256Gb

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
